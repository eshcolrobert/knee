import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { DeviceMotion, DeviceMotionAccelerationData, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion/ngx';
import { GlobalProvider } from '../../providers/global';
import { NativeAudio } from '@ionic-native/native-audio/ngx';

@Component({
  selector: 'app-cstest',
  templateUrl: './cstest.page.html',
  styleUrls: ['./cstest.page.scss'],
})
export class CstestPage {

  msgSit = "Tap to calibrate";
  msg = "Please fasten the device to your thigh, with the screen facing you. Tap here when you are ready to start the test";

  standcalibration = false;

  n = 30
  timevalue = this.n;
  timer: any;
  counter = 0;
  title = "30-sec Chair Stand";
  started = false;

  angle: Number;
  ax: Number;
  axl: Number;
  axh: Number;
  ay: Number;
  ayl: Number;
  ayh: Number;
  az: Number;
  adeg: Number = 90;
  bdeg: Number = 90;
  cdeg: Number = 90;
  extension: Number = 0;
  flexion: Number;
  timestamp: String = "";

  pd = [];
  devId: any;
  dbin: boolean = false;
  pdcount = 0;

  stood = false;
  sat = false;

  step = 0;
  testing = 0;

  constructor(private deviceMotion: DeviceMotion,
    public g: GlobalProvider, private nativeAudio: NativeAudio,
    public platform: Platform) {

    console.log("Sine Inverse of 90 is: ", Math.asin(1));
    console.log("This is equal to " + (180/3.14) * (Math.asin(1)) + " degrees")
    console.log("Square of 4 is ", Math.sqrt(4));
    console.log("It is " + this.adeg + " degrees");

    this.platform.ready().then(() => {	
      this.nativeAudio.preloadComplex('beep', 'assets/media/beep.mp3', 1, 1, 0).then(() => {     
        console.log("Beep ready");
      });
      this.nativeAudio.preloadComplex('sitcalibrate', 'assets/media/sitcalibrate.mp3', 1, 1, 0).then(() => {     
        console.log("sitcalibrate ready");
      });
      this.nativeAudio.preloadComplex('standcalibrate', 'assets/media/standcalibrate.mp3', 1, 1, 0).then(() => {     
        console.log("standcalibrate ready");
      });
      this.nativeAudio.preloadComplex('sitcalibcomplete', 'assets/media/sitcalibcomplete.mp3', 1, 1, 0).then(() => {     
        console.log("sitcalibcomplete ready");
      });
      this.nativeAudio.preloadComplex('standcalibcomplete', 'assets/media/standcalibcomplete.mp3', 1, 1, 0).then(() => {     
        console.log("standcalibcomplete ready");
      });
      this.nativeAudio.preloadComplex('begintest', 'assets/media/begintest.mp3', 1, 1, 0).then(() => {     
        console.log("begintest ready");
      });
      this.nativeAudio.preloadComplex('testdone', 'assets/media/testdone.mp3', 1, 1, 0).then(() => {     
        console.log("testdone ready");
      });
    });

    this.initDev()
      
    }

  prepareTest() {
    this.step = 1;
    this.msg = "Please remain seated while we calibrate the device ..."
    this.nativeAudio.play('sitcalibrate', function(){
      console.log("sitcalibrate Audio played");
    });
    this.sit();
  }

  ionViewDidLoad() {
    this.msg = "Please fasten the device to your thigh, with the screen facing you";
  }

  shortBeep() {
    // this.nativeAudio.play('uniqueKey1', function(){
    //   this.nativeAudio.stop('uniqueKey1');
    // });

    this.nativeAudio.play('beep', function(){
      console.log("Audio played");
    });
  }

  startTest() {
    this.testing = 2;
    var that = this;
    this.started = true;
    console.log("Started Test");
    this.sitstand();
    that.timer = setInterval(function(){
      that.timevalue = that.timevalue - 1;
      console.log("Timevalue is ", that.timevalue);
      if(that.timevalue == 0) {
        that.stopTest();
        that.stop();
        // that.timevalue = that.n;
        that.nativeAudio.play('testdone', function(){
          console.log("testdone Audio played");
        });
      }
    }, 1000)
  }

  stopTest() {
    clearInterval(this.timer);
    this.started = false;
    // this.stop();
  }

  incrCounter() {
    this.counter += 1;
    // this.sat = false;
    // this.stood = false;
    this.shortBeep();
  }

  decrCounter() {
    if(this.counter > 0) {
      this.counter -= 1;
    }
  }

  getAngle(a: Number, b: Number, c: Number) {
    
    var b2: Number;
    var c2: Number;
    var deg: Number;

    b2 = Number(b) * Number(b);
    c2 = Number(c) * Number(c);
    
    deg = Math.round((180 / 3.14) * Math.atan(Number(a)/Math.sqrt(b2.valueOf() + c2.valueOf())));

    return deg;
  }

  initDev() {
    this.started = true;
    // this.axl = 0;
    // this.axh = 0;
    
    try {
      var devOption: DeviceMotionAccelerometerOptions = {
        frequency: 200
      };

      this.devId = this.deviceMotion.watchAcceleration(devOption).subscribe((acc: DeviceMotionAccelerationData) => {
        
              this.ax = Number(acc.x.toFixed(2));
              this.ay = Number(acc.y.toFixed(2));

              this.devId.unsubscribe();
      });

      
    } catch (err) {
      alert("Error " + err);
    }
  }

  start() {
    this.started = true;
    // this.axl = 0;
    // this.axh = 0;
    
    try {
      var devOption: DeviceMotionAccelerometerOptions = {
        frequency: 200
      };

      this.devId = this.deviceMotion.watchAcceleration(devOption).subscribe((acc: DeviceMotionAccelerationData) => {
        
        // if (Number(acc.x.toFixed(2)) > 2) {
        //   if (Number(acc.x.toFixed(2)) > this.ax) {
        //     this.dbin = false;
        //     this.ax = Number(acc.x.toFixed(2));
        //     this.ay = Number(acc.y.toFixed(2));
        //     this.az = Number(acc.z.toFixed(2));
        //   } else {
        //     if (this.dbin == false) {
        //       this.timestamp = acc.timestamp;
        //       this.pd.push({"pid": "1234", "x": this.ax, "y": this.ay, "z": this.az, "timestamp": this.timestamp })
        //       this.pdcount += 1;
        //       this.dbin = true;
        //     } else {
              this.ax = Number(acc.x.toFixed(2));
              this.ay = Number(acc.y.toFixed(2));
              
              this.setvalue();

              // if(!(this.axl == 0) && !(this.axh == 0)) {
                
              // }

              if((this.ay < -8.50) && (this.standcalibration == false)) {
                this.stand();
                this.standcalibration = true;
                this.msg = "Calibrating device for standing position"
              }

              
              this.az = Number(acc.z.toFixed(2));
              
              // this.timestamp = acc.timestamp;

              // this.adeg = this.getAngle(this.ax, this.ay, this.az);
              // this.bdeg = this.getAngle(this.ay, this.ax, this.az);
              // this.cdeg = this.getAngle(this.az, this.ax, this.ay);
    
              // if (this.adeg <= 0) {
              //   this.angle = -1 * Number(this.bdeg)  
              // }

              // if (this.adeg > 0) {
              //   this.angle = 90 + Number(this.adeg)  
              // }

              // this.flexion = this.angle;

              // this.pd.push({"pid": "1234", "x": this.ax, "y": this.ay, "z": this.az, "timestamp": this.timestamp })
          //   }
            
          //   if (this.pdcount == 3) {
          //     this.send();
          //     this.pdcount = 0;
          //   }
            
          // }  
        // }
        

      });
      
    } catch (err) {
      alert("Error " + err);
    }
  }

  setvalue() {
    if((this.axl == null) && (this.axh == null)) {
      this.axl = this.ax;
      this.ayl = this.ay;
      this.axh = this.ax;
      this.ayh = this.ay;
    } else {
      if((this.ax > this.axl) && (this.ax > this.axh)) {
        this.axh = this.ax
      } else if(this.ax < this.axl) {
        this.axl = this.ax
      }

      if((this.ay > this.ayh) && (this.ay > this.ayh)) {
        this.ayh = this.ay
      } else if(this.ay < this.ayl) {
        this.ayl = this.ay
      }
    }
  }

  sit() {
    this.msg = "Calibrating device while seated ..."
    this.axl = this.axh = this.ayl = this.ayh = null;
    var that = this;
    this.start();
    var sittime = 10;
    this.timer = setInterval(function(){
      sittime -= 1;
      console.log("Sit Time is ", sittime);
      if(sittime == 5) {
        that.g.xli = Number(that.axl);
        that.g.xhi = Number(that.axh);
        that.g.yli = Number(that.ayl);
        that.g.yhi = Number(that.ayh);
        console.log("g.xli: " + that.g.xli + "g.xhi: " + that.g.xhi + "g.yli: " + that.g.yli + "g.yhi: " + that.g.yhi);
        // that.shortBeep();
        that.nativeAudio.play('sitcalibcomplete', function(){
          console.log("sitcalibcomplete Audio played");
        })
      }

      if(sittime == 0) {
        that.msg = "Please stand for the next calibration"
        that.step = 2;
        that.stopTest();
        that.nativeAudio.play('standcalibrate', function(){
          console.log("standcalibrate Audio played");
        });
      }
    }, 1000)
  }

  stand() {
    var that = this;
    // this.start();
    var standtime = 10;
    this.timer = setInterval(function(){
      standtime -= 1;
      console.log("Stand Time is ", standtime);
      if(standtime == 5) {
        that.g.xla = Number(that.axl);
        that.g.xha = Number(that.axh);
        that.g.yla = Number(that.ayl);
        that.g.yha = Number(that.ayh);
        console.log("g.xla: " + that.g.xla + "g.xha: " + that.g.xha + "g.yla: " + that.g.yla + "g.yha: " + that.g.yha);
        that.axl = that.axh = that.ayl = that.ayh = null;
        that.msg = "Standing calibration done!"
        that.nativeAudio.play('standcalibcomplete', function(){
          console.log("standcalibcomplete Audio played");
        });
        // that.shortBeep();
      }

      if(standtime == 0) {
        that.stopTest();
        that.stop();
        that.nativeAudio.play('begintest', function(){
          console.log("begintest Audio played");
        });    
        that.step = 3;
        that.testing = 1;
      }
    }, 1000)
  }

  stop() {
    this.started = false;
    this.devId.unsubscribe();
  }

  send() {
    let input = this.pd;
    input = JSON.parse(JSON.stringify(input));
    this.pd = [];
    this.g.postCallLambda("https://9o1t7jvzg3.execute-api.us-east-1.amazonaws.com/prod/savegksdata", {"data": input}).then(data => {
      console.log("Response from Server: ", JSON.stringify(data));
    }).catch(error => {
      console.log("Error: ", JSON.stringify(error));
    })
  }

  setExtension() {
    // localStorage.setItem("extension", String(this.angle));
    // this.extension = Number(localStorage.getItem("extension"));
    this.extension = this.angle;
  }

  sitstand() {

    var stood = 0;
    var sat = 0;

    try {
      var devOption: DeviceMotionAccelerometerOptions = {
        frequency: 200
      };

      this.devId = this.deviceMotion.watchAcceleration(devOption).subscribe((acc: DeviceMotionAccelerationData) => {
        this.ax = Number(acc.x.toFixed(2));
        this.ay = Number(acc.y.toFixed(2));

        if(this.stood == false) {
          console.log("Stood False");
          if((this.ay <= this.g.yla)) {
            // if((this.ay >= this.g.yla) && (this.ay <= this.g.yha)) {
              this.stood = true;
              stood += 1;
              console.log("Stood ", stood);
            // }
          }
        } else if(this.sat == false) {
          console.log("Sat False");
          if(this.ay >= this.g.yli){
            // if((this.ay >= this.g.yli) && (this.ay <= this.g.yhi)) {
              this.sat = true;
              sat += 1;
              console.log("Sat ", sat);
            // }
          }
        } else if((this.sat == true) && (this.stood == true)) {
          console.log("Stood True");
          console.log("Sat True");
          this.incrCounter();
          this.sat = false;
          this.stood = false;
        }

        console.log("ay: " + this.ay + " g.yli: " + this.g.yli + " g.yla: " + this.g.yla);

        // if(this.stood == false) {
        //   if((this.ax > this.g.xla) && (this.ax > this.g.xha)) {
        //     this.stood = true;
        //   }
        // }


        // if((this.ax > this.g.xla) && (this.ax > this.g.xha)) {
        //   this.axh = this.ax
        // } else if(this.ax < this.axl) {
        //   this.axl = this.ax
        // }

        // if(this.ay > this.ayh) {
        //   this.ayh = this.ay
        // } else if(this.ay < this.ayl) {
        //   this.ayl = this.ay
        // }
        
        //       this.ay = Number(acc.y.toFixed(2));
        //       this.az = Number(acc.z.toFixed(2));
      });
    } catch(err) {
      console.log("Error sitstand")
    }
  }

  restart() {
    this.stopTest();
    this.stop();
    this.step = 0;
    this.testing = 0;
    this.msg = "Please fasten the device to your thigh, with the screen facing you. Tap here when you are ready to start the test";
    this.standcalibration = false;
    this.timevalue = this.n;
    this.stood = false;
    this.sat = false;
  }

}
