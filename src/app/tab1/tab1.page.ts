import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DeviceMotion, DeviceMotionAccelerationData, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion/ngx';
import { GlobalProvider } from '../../providers/global';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  
  angle: Number;
  ax: Number;
  ay: Number;
  az: Number;
  adeg: Number = 90;
  bdeg: Number = 90;
  cdeg: Number = 90;
  extension: Number = 0;
  flexion: Number;
  timestamp: String = "";

  pd = [];
  devId: any;
  dbin: boolean = false;
  pdcount = 0;
  started = false;

  constructor(public navCtrl: NavController, private deviceMotion: DeviceMotion,
              public g: GlobalProvider,
              public barcodeScanner: BarcodeScanner) {

    console.log("Sine Inverse of 90 is: ", Math.asin(1));
    console.log("This is equal to " + (180/3.14) * (Math.asin(1)) + " degrees")
    console.log("Square of 4 is ", Math.sqrt(4));
    console.log("It is " + this.adeg + " degrees");
  }

  scanBarCode() {
    console.log("Scan barcode function executed");
    this.barcodeScanner.scan().then(barcodeData => {
      if(barcodeData.text == "1234567890") {
        this.g.patient = barcodeData.text;
      }
      console.log('Barcode data', barcodeData);
     }).catch(err => {
         console.log('Error', err);
     });
  }

  getAngle(a: Number, b: Number, c: Number) {
    
    var b2: Number;
    var c2: Number;
    var deg: Number;

    b2 = Number(b) * Number(b);
    c2 = Number(c) * Number(c);
    
    deg = Math.round((180 / 3.14) * Math.atan(Number(a)/Math.sqrt(b2.valueOf() + c2.valueOf())));

    return deg;
  }

  start() {
    this.started = true;
    try {
      var devOption: DeviceMotionAccelerometerOptions = {
        frequency: 200
      };

      this.devId = this.deviceMotion.watchAcceleration(devOption).subscribe((acc: DeviceMotionAccelerationData) => {
        
        // if (Number(acc.x.toFixed(2)) > 2) {
        //   if (Number(acc.x.toFixed(2)) > this.ax) {
        //     this.dbin = false;
        //     this.ax = Number(acc.x.toFixed(2));
        //     this.ay = Number(acc.y.toFixed(2));
        //     this.az = Number(acc.z.toFixed(2));
        //   } else {
        //     if (this.dbin == false) {
        //       this.timestamp = acc.timestamp;
        //       this.pd.push({"pid": "1234", "x": this.ax, "y": this.ay, "z": this.az, "timestamp": this.timestamp })
        //       this.pdcount += 1;
        //       this.dbin = true;
        //     } else {
              this.ax = Number(acc.x.toFixed(2));
              this.ay = Number(acc.y.toFixed(2));
              this.az = Number(acc.z.toFixed(2));
              
              this.timestamp = acc.timestamp;

              this.adeg = this.getAngle(this.ax, this.ay, this.az);
              this.bdeg = this.getAngle(this.ay, this.ax, this.az);
              this.cdeg = this.getAngle(this.az, this.ax, this.ay);
    
              if (this.adeg <= 0) {
                this.angle = -1 * Number(this.bdeg)  
              }

              if (this.adeg > 0) {
                this.angle = 90 + Number(this.adeg)  
              }

              this.flexion = this.angle;

              this.pd.push({"pid": "1234", "x": this.ax, "y": this.ay, "z": this.az, "timestamp": this.timestamp })
          //   }
            
          //   if (this.pdcount == 3) {
          //     this.send();
          //     this.pdcount = 0;
          //   }
            
          // }  
        // }
        

      });
      
    } catch (err) {
      alert("Error " + err);
    }
  }

  stop() {
    this.started = false;
    this.devId.unsubscribe();
  }

  send() {
    let input = this.pd;
    input = JSON.parse(JSON.stringify(input));
    this.pd = [];
    this.g.postCallLambda("https://9o1t7jvzg3.execute-api.us-east-1.amazonaws.com/prod/savegksdata", {"data": input}).then(data => {
      console.log("Response from Server: ", JSON.stringify(data));
    }).catch(error => {
      console.log("Error: ", JSON.stringify(error));
    })
  }

  setExtension() {
    // localStorage.setItem("extension", String(this.angle));
    // this.extension = Number(localStorage.getItem("extension"));
    this.extension = this.angle;
  }
}
