import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DeviceMotion, DeviceMotionAccelerationData, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion/ngx';
import { GlobalProvider } from '../../providers/global';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  title = "";

  constructor(public navCtrl: NavController, private deviceMotion: DeviceMotion,
    public g: GlobalProvider, public router: Router) {
      this.title = "hey";
    }

  cstest() {
    this.router.navigateByUrl("tabs/tab2/cstest");
  }

}
