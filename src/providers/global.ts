import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
// import { Platform, normalizeURL, ToastController, LoadingController, Events, App } from 'ionic-angular';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class GlobalProvider {

    pp = {
        "id": "1234567890",
        "name": "Smith, John J",
        "hospital_name": "Einstein Memorial Hospital",
        "hospital_location": "Norristown PA 19401",
        "surgeon": "Robert Frost MD"
    };

    patient = "";

    xli = 0;
    xhi = 0;
    yli = 0;
    yhi = 0;

    xla = 0;
    xha = 0;
    yla = 0;
    yha = 0;

    constructor(public http: HttpClient) {

    }

    postCallLambda(url: any, obj: any) {
        return new Promise((resolve, reject)=>{
            this.http.post(url, obj)
            .pipe(map(res => {}))
            .subscribe((data: any) => {
                if(data && data.body){
                    resolve(JSON.parse(data.body));
                }
                else{
                resolve([]);
                }
            }); 
        });
    }
}